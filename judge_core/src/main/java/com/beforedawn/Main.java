package com.beforedawn;

import com.beforedawn.compare.Compare;
import com.beforedawn.compare.CompareNormal;
import com.beforedawn.compile.Compile;
import com.beforedawn.compile.c.Cpp17CompileCommand;
import com.beforedawn.compile.c.NullStandard;
import com.beforedawn.compile.c.Standard;
import com.beforedawn.judge.Judge;
import com.beforedawn.judge.Report;
import com.beforedawn.run.ExeRunCommand;
import com.beforedawn.run.Run;
import com.beforedawn.util.RunData;

/**
 * <p>
 * 判题程序
 * </p>
 * <p>
 * 最低java支持版本 9
 * 推荐java版本 11
 * </p>
 * <p>
 * 可运行系统：
 * <ul>
 *     <li>windows</li>
 *     <li>linux</li>
 * </ul>
 * </p>
 *
 * @author moyulingjiu
 * @date 2021年11月25日
 * @since 0.1.alpha
 */
public class Main {
    public static void main(String[] args) {
        String sourcePath = args[0];
        String inputDir = args[1];
        String outputDir = args[2];
        long millisecond = Long.parseLong(args[3]);
        long mb = Long.parseLong(args[4]);

//        Compile compile;
//        Run run;
        Compare compare = new CompareNormal();
//
//        if (args.length > 5) {
//            String language = args[5];
//            if (language.startsWith("c")) {
//                Standard standard = new NullStandard();
//                run = new Run(new ExeRunCommand());
//                if (language.equals("cpp17")) {
//
//                }
//            }
//        }

        Judge judge = new Judge(new Compile(new Cpp17CompileCommand()), new Run(new ExeRunCommand()), compare);
        Report report = judge.handle(sourcePath, inputDir, outputDir, new RunData(millisecond, mb));
        System.out.println(report);
    }
}
