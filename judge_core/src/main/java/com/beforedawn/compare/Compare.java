package com.beforedawn.compare;

import com.beforedawn.judge.Report;

public abstract class Compare {
    public abstract Report handle(Report report, String inputDir, String outputDir);
}
