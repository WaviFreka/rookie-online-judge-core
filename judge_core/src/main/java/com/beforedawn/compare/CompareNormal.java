package com.beforedawn.compare;

import com.beforedawn.judge.Report;
import com.beforedawn.judge.ReportState;
import com.beforedawn.judge.TestNode;
import com.beforedawn.util.FileIterator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 一般判题
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class CompareNormal extends Compare {
    @Override
    public Report handle(Report report, String inputDir, String outputDir) {
        File input = new File(inputDir);
        File[] inputFiles = input.listFiles();
        File output = new File(outputDir);
        File[] outputFiles = output.listFiles();
        if (inputFiles == null || outputFiles == null) {
            report.clear();
            report.addTestNode(new TestNode("0", ReportState.SE));
            return report;
        }
        Map<String, File> origin = getValidMap(inputFiles);
        Map<String, File> compare = getValidMap(outputFiles);

        for (TestNode node : report.getNodes()) { // 评判每个节点
            if (node.getState() == ReportState.AC && origin.containsKey(node.getFilename()) && compare.containsKey(node.getFilename())) {
                FileIterator fileIterator1 = new FileIterator(origin.get(node.getFilename()));
                FileIterator fileIterator2 = new FileIterator(compare.get(node.getFilename()));
                String line1, line2 = null;
                while ((line1 = fileIterator1.next()) != null && (line2 = fileIterator2.next()) != null) {
                    if (!line1.equals(line2)) {
                        node.setState(ReportState.WA);
                        node.addLineDifferent(new LineDifferent(
                                fileIterator1.getLineNumber(),
                                line1,
                                fileIterator2.getLineNumber(),
                                line2));
                    }
                }
                if (fileIterator1.getLineNumber() != fileIterator2.getLineNumber() ||
                        (line1 = fileIterator1.next()) != null ||
                        (line2 = fileIterator2.next()) != null
                        ) { // 文件不等长
                    node.setState(ReportState.WA);
                    node.addLineDifferent(new LineDifferent(
                            fileIterator1.getLineNumber(),
                            line1,
                            fileIterator2.getLineNumber(),
                            line2));
                }
                if (node.getState() != ReportState.AC)
                    report.setState(ReportState.WA);
            } else {
                node.setState(ReportState.SE);
                report.setState(ReportState.SE);
            }
        }
        return report;
    }

    private static Map<String, File> getValidMap(File[] Files) {
        Map<String, File> valid = new HashMap<>();
        for (File file : Files) {
            if (!file.isDirectory() && file.toString().endsWith(".out")) {
                valid.put(file.getName().substring(0, file.getName().lastIndexOf(".")), file);
            }
        }
        return valid;
    }
}
