package com.beforedawn.compare;

/**
 * 行内差异
 *
 * @author moyulingjiu
 * @date 2021年11月25日
 */
public class LineDifferent {
    int lineNumberOrigin;
    String origin; // 原始文本
    int lineNumberCompare;
    String compare; // 对比的文本

    public LineDifferent() {
        lineNumberOrigin = 0;
        origin = "";
        lineNumberCompare = 0;
        compare = "";
    }

    public LineDifferent(int lineNumberOrigin, String origin, int lineNumberCompare, String compare) {
        this.lineNumberOrigin = lineNumberOrigin;
        this.origin = origin;
        this.lineNumberCompare = lineNumberCompare;
        this.compare = compare;
    }

    public int getLineNumberOrigin() {
        return lineNumberOrigin;
    }

    public void setLineNumberOrigin(int lineNumberOrigin) {
        this.lineNumberOrigin = lineNumberOrigin;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getLineNumberCompare() {
        return lineNumberCompare;
    }

    public void setLineNumberCompare(int lineNumberCompare) {
        this.lineNumberCompare = lineNumberCompare;
    }

    public String getCompare() {
        return compare;
    }

    public void setCompare(String compare) {
        this.compare = compare;
    }

    @Override
    public String toString() {
        return String.format("+@%d:%s\n-@%d:%s\n", lineNumberOrigin, origin, lineNumberCompare, compare);
    }
}
