package com.beforedawn.compile;

/**
 * 编译指令的基类
 *
 * @author Moyulingjiu
 * @date 2021年11月23日
 */
public abstract class CompileCommand {
    public abstract String getCompileCommand();
}
