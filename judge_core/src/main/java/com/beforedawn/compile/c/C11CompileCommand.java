package com.beforedawn.compile.c;

/**
 * C11标准的编译
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class C11CompileCommand extends CLanguageCompileCommand {
    public C11CompileCommand() {
    }

    public C11CompileCommand(Standard standard) {
        this.standard = standard;
    }

    @Override
    public String getCompileCommand() {
        return "gcc {filename}.c -o {filename} -fno-asm -fmax-errors=10 -Wall -lm --static -std=c11" + standard.getStander();
    }
}
