package com.beforedawn.compile.c;

/**
 * C99标准的编译
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class C95CompileCommand extends CLanguageCompileCommand {
    public C95CompileCommand() {
    }

    public C95CompileCommand(Standard standard) {
        this.standard = standard;
    }

    @Override
    public String getCompileCommand() {
        return "gcc {filename}.c -o {filename} -fno-asm -fmax-errors=10 -Wall -lm --static -std=c95" + standard.getStander();
    }
}
