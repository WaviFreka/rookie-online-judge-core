package com.beforedawn.compile.c;

/**
 * C99标准的编译
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class C99CompileCommand extends CLanguageCompileCommand {
    public C99CompileCommand() {
    }

    public C99CompileCommand(Standard standard) {
        this.standard = standard;
    }

    @Override
    public String getCompileCommand() {
        return "gcc {filename}.c -o {filename} -fno-asm -fmax-errors=10 -Wall -lm --static -std=c99" + standard.getStander();
    }
}
