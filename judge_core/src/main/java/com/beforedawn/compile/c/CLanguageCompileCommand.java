package com.beforedawn.compile.c;

import com.beforedawn.compile.CompileCommand;

/**
 * C系语言的抽象类，用于桥接O2优化、O3优化。
 * 默认优化为O1
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public abstract class CLanguageCompileCommand extends CompileCommand {
    protected Standard standard = new NullStandard();
}
