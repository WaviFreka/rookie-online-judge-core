package com.beforedawn.compile.c;

/**
 * C++11标准的编译
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class Cpp11CompileCommand extends CLanguageCompileCommand {
    public Cpp11CompileCommand() {
    }

    public Cpp11CompileCommand(Standard standard) {
        this.standard = standard;
    }

    @Override
    public String getCompileCommand() {
        return "g++ filename.cpp -o filename -fno-asm -fmax-errors=10 -Wall -lm --static -std=c++11" + standard.getStander();
    }
}
