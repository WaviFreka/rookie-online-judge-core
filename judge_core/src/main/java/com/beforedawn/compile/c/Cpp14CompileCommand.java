package com.beforedawn.compile.c;

import com.beforedawn.compile.CompileCommand;

/**
 * C++14标准的编译
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class Cpp14CompileCommand extends CLanguageCompileCommand {
    public Cpp14CompileCommand() {
    }

    public Cpp14CompileCommand(Standard standard) {
        this.standard = standard;
    }

    @Override
    public String getCompileCommand() {
        return "g++ filename.cpp -o filename -fno-asm -fmax-errors=10 -Wall -lm --static -std=c++14" + standard.getStander();
    }
}
