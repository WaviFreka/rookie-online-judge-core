package com.beforedawn.compile.c;

import com.beforedawn.util.JudgeSystem;

/**
 * C++17标准的编译
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class Cpp17CompileCommand extends CLanguageCompileCommand {
    public Cpp17CompileCommand() {
    }

    public Cpp17CompileCommand(Standard standard) {
        this.standard = standard;
    }

    @Override
    public String getCompileCommand() {
        if (JudgeSystem.isWindows())
            return "g++ filename.cpp -o filename -fno-asm -fmax-errors=10 -Wall -lm --static -std=c++17" + standard.getStander();
        else
            return "g++ filename.cpp -o filename -Wall -L/usr/lib64 -lm -std=c++17" + standard.getStander();
    }
}
