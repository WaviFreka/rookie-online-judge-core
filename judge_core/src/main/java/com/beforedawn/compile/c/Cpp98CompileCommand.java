package com.beforedawn.compile.c;

/**
 * C++98标准的编译
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class Cpp98CompileCommand extends CLanguageCompileCommand {
    public Cpp98CompileCommand() {
    }

    public Cpp98CompileCommand(Standard standard) {
        this.standard = standard;
    }

    @Override
    public String getCompileCommand() {
        return "g++ filename.cpp -o filename -fno-asm -fmax-errors=10 -Wall -lm --static -std=c++98" + standard.getStander();
    }
}
