package com.beforedawn.compile.c;

/**
 * 空标准，即采用默认标准
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class NullStandard extends Standard {
    @Override
    public String getStander() {
        return "";
    }
}
