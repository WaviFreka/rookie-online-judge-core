package com.beforedawn.compile.c;

/**
 * O2优化的命令
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class O2Standard extends Standard {
    @Override
    public String getStander() {
        return " -O2";
    }
}
