package com.beforedawn.compile.c;

/**
 * C系语言编译标准的抽象类
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public abstract class Standard {
    public abstract String getStander();
}
