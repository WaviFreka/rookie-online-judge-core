package com.beforedawn.compile.java;

import com.beforedawn.compile.CompileCommand;

/**
 * java的编译命令
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class JavaCompileCommand extends CompileCommand {
    @Override
    public String getCompileCommand() {
        return "javac -J-Xms32m -J-Xmx256m -encoding UTF-8 filename.java";
    }
}
