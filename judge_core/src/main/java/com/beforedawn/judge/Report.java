package com.beforedawn.judge;

import java.util.ArrayList;
import java.util.List;

/**
 * 判题报告
 *
 * @author moyulingjiu
 * @date 2021年11月25日
 */
public class Report {
    private List<TestNode> nodes = new ArrayList<>();
    private ReportState state; // 主状态

    public Report() {
        state = ReportState.AC;
    }

    public Report(ReportState state) {
        this.state = state;
    }

    public void addTestNode(TestNode node) {
        nodes.add(node);
    }

    public void setTestNode(int index, TestNode node) {
        nodes.set(index, node);
    }

    public List<TestNode> getNodes() {
        return nodes;
    }

    public void clear() {
        nodes = new ArrayList<>();
    }

    public ReportState getState() {
        return state;
    }

    public void setState(ReportState state) {
        this.state = state;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("total: ").append(state).append("\n");
        for (TestNode node : nodes) {
            builder.append(node.toString());
        }
        return builder.toString();
    }
}
