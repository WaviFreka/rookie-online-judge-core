package com.beforedawn.judge;

import com.beforedawn.compile.CompileState;

/**
 * 返回值类型
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public enum ReportState {
    AC("答案正确", "Accepted"),
    WA("答案错误", "Wrong Answer"),
    RE("运行错误", "Runtime Error"),
    PE("格式错误", "Presentation Error"),
    TLE("时间超限", "Time Limit Exceeded"),
    MLE("内存超限", "Memory Limit Exceeded"),
    OLE("输出超限", "Output Limit Exceeded"),
    CE("编译错误", "Compile Error"),
    SE("系统错误", "System Error"),
    VE("危险代码", "Validator Error"); // 最后这一行根据标准是有这个错误，但是这里应该不做返回，直接使用docker屏蔽危险代码

    private final String name;
    private final String state;

    ReportState(String name, String state) {
        this.name = name;
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public String getState() {
        return state;
    }

    public static ReportState decorate(CompileState compileState) {
        if (compileState == CompileState.FAIL) {
            return CE;
        }
        return SE;
    }
}
