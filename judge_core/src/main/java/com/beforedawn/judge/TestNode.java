package com.beforedawn.judge;

import com.beforedawn.compare.LineDifferent;
import com.beforedawn.util.RunData;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试节点的详细报告
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class TestNode {
    private static int originIndex = 1;
    private final int index; // 测试点序号
    private ReportState state; // 测试点状态
    private List<LineDifferent> lines = new ArrayList<>(); // 错误对比
    private RunData runData = new RunData(0, 0); // 运行数据
    private String filename; // 对应的测试点信息

    private String getPureName(String name) {
        if (name.contains("/"))
            name = name.substring(name.lastIndexOf("/") + 1);
        else
            name = name.substring(name.lastIndexOf("\\") + 1);
        return name.substring(0, name.lastIndexOf("."));
    }

    public TestNode() {
        index = originIndex++;
        this.state = ReportState.AC;
    }

    public TestNode(String filename) {
        index = originIndex++;
        this.state = ReportState.AC;
        this.filename = getPureName(filename);
    }

    public TestNode(String filename, ReportState state) {
        index = originIndex++;
        this.state = state;
        this.filename = getPureName(filename);
    }

    public TestNode(String filename, ReportState state, RunData runData) {
        index = originIndex++;
        this.state = state;
        this.filename = getPureName(filename);
        this.runData = runData;
    }

    public int getIndex() {
        return index;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public ReportState getState() {
        return state;
    }

    public void setState(ReportState state) {
        this.state = state;
    }


    public void addLineDifferent(LineDifferent lineDifferent) {
        lines.add(lineDifferent);
    }

    public List<LineDifferent> getLines() {
        return lines;
    }

    public void setLines(List<LineDifferent> lines) {
        this.lines = lines;
    }

    public RunData getRunData() {
        return runData;
    }

    public void setRunData(RunData runData) {
        this.runData = runData;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("test #")
                .append(index)
                .append(" :")
                .append(state.toString())
                .append(" ")
                .append(runData.getMillisecond())
                .append("ms ")
                .append(runData.getMB())
                .append("MB")
                .append("\n");
        for (LineDifferent line : lines) {
            builder.append(line.toString());
        }
        return builder.toString();
    }
}
