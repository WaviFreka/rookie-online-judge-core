package com.beforedawn.run;

import com.beforedawn.util.JudgeSystem;

/**
 * 可执行文件的运行命令
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class ExeRunCommand extends RunCommand {
    @Override
    public String getRunCommand() {
        if (JudgeSystem.isWindows())
            return "filename.exe";
        else if (JudgeSystem.isLinux())
            return "filename";
        return "";
    }
}
