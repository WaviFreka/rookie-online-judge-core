package com.beforedawn.run;

/**
 * jar包运行命令
 *
 * @author moyulingjiu
 * @date 2021年11月25日
 */
public class JarRunCommand extends RunCommand {
    @Override
    public String getRunCommand() {
        return "java -jar filename.jar";
    }
}
