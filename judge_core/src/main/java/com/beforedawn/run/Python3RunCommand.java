package com.beforedawn.run;

import com.beforedawn.util.JudgeSystem;

/**
 * python3运行命令
 *
 * @author moyulingjiu
 * @date 2021年11月25日
 */
public class Python3RunCommand extends RunCommand {

    @Override
    public String getRunCommand() {
        if (JudgeSystem.isWindows()) {
            return "python filename.py";
        } else {
            return "python3 filename.py";
        }
    }
}
