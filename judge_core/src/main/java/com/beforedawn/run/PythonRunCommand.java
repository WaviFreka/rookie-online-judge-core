package com.beforedawn.run;

/**
 * python2运行命令
 *
 * @author moyulingjiu
 * @date 2021年11月25日
 */
public class PythonRunCommand extends RunCommand {
    @Override
    public String getRunCommand() {
        return "python filename.py";
    }
}
