package com.beforedawn.run;

/**
 * 运行命令抽象类
 *
 * @author moyulingjiu
 * @date 2021年11月25日
 */
public abstract class RunCommand {
    public abstract String getRunCommand();
}
