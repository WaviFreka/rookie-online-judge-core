package com.beforedawn.util;

import java.io.File;

/**
 * 递归删除一个文件夹下所有文件，并返回结果
 *
 * @author moyulingjiu
 * @date 2021年11月25日
 */
public class DeleteFile {
    private static boolean flag = true;//用来判断文件是否删除成功

    public static boolean handle(File file) {
        //判断文件不为null或文件目录存在
        if (file == null || !file.exists()) {
            return true;
        }
        //取得这个目录下的所有子文件对象
        File[] files = file.listFiles();
        //遍历该目录下的文件对象
        if (files == null)
            return true;
        for (File f : files) {
            //打印文件名
            String name = file.getName();
            //判断子目录是否存在子目录,如果是文件则删除
            if (f.isDirectory()) {
                flag = flag && handle(f);
            } else {
                flag = flag &&  f.delete();
            }
        }
        //删除空文件夹  for循环已经把上一层节点的目录清空。
        flag = flag &&  file.delete();
        return flag;
    }
}
