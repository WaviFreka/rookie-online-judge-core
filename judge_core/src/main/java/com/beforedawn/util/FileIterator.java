package com.beforedawn.util;

import java.io.*;

public class FileIterator {
    private BufferedReader reader;
    private int lineNumber;

    public FileIterator(BufferedReader reader) {
        this.reader = reader;
        lineNumber = 0;
    }

    public FileIterator(File file) {
        lineNumber = 0;
        try {
            reader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public FileIterator(String path) {
        lineNumber = 0;
        try {
            reader = new BufferedReader(new FileReader(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public String next() {
        String line = null;
        try {
            line = reader.readLine();
            if (line != null) {
                lineNumber++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }
}
