package com.beforedawn.util;

/**
 * 判定当前的系统
 *
 * @author moyulingjiu
 * @date 2021年11月23日
 */
public class JudgeSystem {
    public static boolean isLinux() {
        return System.getProperty("os.name").toLowerCase().contains("linux");
    }

    public static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }

    public static String getSystemName() {
        if (isLinux()) {
            return "linux";
        } else if (isWindows()) {
            return "windows";
        } else {
            return "other system";
        }
    }

    public static String getFullSystemName() {
        return System.getProperty("os.name");
    }
}
