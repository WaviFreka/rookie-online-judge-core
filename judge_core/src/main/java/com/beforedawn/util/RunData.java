package com.beforedawn.util;

public class RunData {
    private long millisecond;
    private long MB;

    public RunData(long millisecond, long MB) {
        this.millisecond = millisecond;
        this.MB = MB;
    }

    public long getMillisecond() {
        return millisecond;
    }

    public void setMillisecond(long millisecond) {
        this.millisecond = millisecond;
    }

    public long getMB() {
        return MB;
    }

    public void setMB(long MB) {
        this.MB = MB;
    }
}
